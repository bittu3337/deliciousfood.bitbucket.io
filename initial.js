(()=> {
	const myDiv = document.getElementById('my-div');


	const data = [
		{
			imgurl:'https://images.pexels.com/photos/2983098/pexels-photo-2983098.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name: 'Burger',
		},
		{
			imgurl:'https://images.pexels.com/photos/479628/pexels-photo-479628.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Chips',
		},
		{
			imgurl:'https://images.pexels.com/photos/6157093/pexels-photo-6157093.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Coconut',
			description:"Coconut is the fruit of the coconut palm (Cocos nucifera). It's used for its water, milk, oil, and tasty meat"
		},
		{
			imgurl:'https://images.pexels.com/photos/6659881/pexels-photo-6659881.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Peanut',
			description:'It is widely grown in the tropics and subtropics, being important to both small and large commercial producer'
		},
		{
			imgurl:'https://images.pexels.com/photos/4820675/pexels-photo-4820675.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Beans',
			description:'A bean is the seed of one of several genera of the flowering plant family Fabaceae, which are used as vegetables for human or animal food'
		},
		{
			imgurl:'https://images.pexels.com/photos/3029520/pexels-photo-3029520.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Avocado',
			description:'The fruit of the plant, also called an avocado, is botanically a large berry containing a single large seed'
		},
		{
			imgurl:'https://images.pexels.com/photos/162712/egg-white-food-protein-162712.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Eggs',
			description:'Rich in vitamin D'
		},
		{
			imgurl:'https://images.pexels.com/photos/2129654/pexels-photo-2129654.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Chicken',
			description:'Chicken is the most common type of poultry in the world.'
		},
		{
			imgurl:'https://images.pexels.com/photos/161559/background-bitter-breakfast-bright-161559.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Oranges',
			description:'The orange is the fruit of various citrus species in the family Rutaceae'
		},
		{
			imgurl:'https://images.pexels.com/photos/6646073/pexels-photo-6646073.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Rice',
			description:'Rice is the seed of the grass species Oryza sativa or less commonly Oryza glaberrima.'
		},
		{
			imgurl:'https://images.pexels.com/photos/33783/olive-oil-salad-dressing-cooking-olive.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Cooking Oil',
			description:'Cooking oil is plant, animal, or synthetic fat used in frying, baking, and other types of cooking'
		},
		{
			imgurl:'https://images.pexels.com/photos/1656666/pexels-photo-1656666.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
			name:'Vegetables',
			description:'Vegetables are parts of plants that are consumed by humans or other animals as food'
		}
	];


	let html = '';

	data.forEach((item)=> {
		html += `<article>
					<a href='${item.imgurl}' title='${item.name}'><img src='${item.imgurl}' alt='${item.name}'></a>
					<h3>${item.name}</h3>
					
					<button item-name='${item.name}'>Place Order</button>
				</article>`;
	});
	myDiv.innerHTML = html;


})();